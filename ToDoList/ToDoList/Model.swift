//
//  Model.swift
//  ToDoList
//
//  Created by Gerel Burgustina on 10/12/2019.
//  Copyright © 2019 Gerel Burgustina. All rights reserved.
//

import Foundation

var todos = ["Mom", "Dad", "Sister"]

func addTask (nameTask: String) {
    
    todos.append(nameTask)
    saveData()
    
}

func removeTask (at index: Int) {
    
    todos.remove(at: index)
    saveData()
    
    
}

func saveData() {
    
    
}

func loadData() {
    
    
}
