//
//  ViewController.swift
//  ToDoList
//
//  Created by Gerel Burgustina on 06/12/2019.
//  Copyright © 2019 Gerel Burgustina. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchDisplayDelegate, UISearchBarDelegate {
    
//    tasks
    
    var todos = ["Mom", "Dad", "Sister"]
    
    
    
    
    @IBOutlet weak var toDoTableView: UITableView!
    
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    
    //    ADD button + alert
    
    @IBAction func addPlus(_ sender: Any) {
        
        let todoAlert = UIAlertController(title: "Add a new task", message: nil, preferredStyle: .alert)
        
        todoAlert.addTextField()
        let todoAction = UIAlertAction(title: "Add", style: .cancel) { (action) in
            
            let newTodo = todoAlert.textFields! [0]
            self.todos.append(newTodo.text!)
            self.toDoTableView.reloadData()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default)
        
        todoAlert.addAction(todoAction)
        todoAlert.addAction(cancelAction)
        
        present(todoAlert, animated: true, completion: nil)
    }
    /*
    @IBAction func addTodo(_ sender: Any) {
        
        let todoAlert = UIAlertController(title: "Creat a new task", message: nil, preferredStyle: .alert)
        
        todoAlert.addTextField()
        let todoAction = UIAlertAction(title: "Add", style: .cancel) { (action) in
            
            let newTodo = todoAlert.textFields! [0]
            self.todos.append(newTodo.text!)
            self.toDoTableView.reloadData()
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default)
        
        todoAlert.addAction(todoAction)
        todoAlert.addAction(cancelAction)
        
        present(todoAlert, animated: true, completion: nil)
    } */
    
//    VIEW did load
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        toDoTableView.delegate = self
        toDoTableView.dataSource = self
        toDoTableView.rowHeight = 50
        
        searchBar.delegate = self
        
         
        
    }
    
    
//Table structure
    
    func numberOfSections(in tableView: UITableView) -> Int {
            return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todos.count
    }
    





     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "ToDoCell", for: indexPath) as! toDoCell

        cell.toDoCellLabel.text = todos[indexPath.row]
    
        return cell
        
    }
    
    
//   CHECKMARK
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let cell = tableView.cellForRow(at: indexPath) as! toDoCell
        
        if cell.isCheked == false {
            cell.checkmarkImage.image = UIImage(named: "checktrans.png")
            cell.isCheked = true
            
        } else {
            cell.checkmarkImage.image = nil
            cell.isCheked = false
        }
    }
    
    
    

    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            todos.remove(at: indexPath.row)
            toDoTableView.reloadData()
            
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//        if searchTodo != "" {
//            
////            var predicate =
//            
//            
//        }
    
}

}
